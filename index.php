<html>
<head>
<title>Island Media Test Phase 1</title>
<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<style>
body{
    background-color:#EDEDED;
    padding:0;
    margin:0;
    font-family:Arial, Helvetica, sans-serif
}
.container{
    width:100%;
    display:block;
    padding:0;
    height:120px;
    margin:-60px 0 0 0;
    position:absolute;
    top:50%;
    text-align:center;
}
.today{
    color:#000;
    width:auto;
}
.today strong{
    color:#017D27;
}
.days{
    background-color:#CBCBCB;
    display:block;
    padding:5px;
    margin:15px 0;
    height:auto;
}
.days a{
    display:inline-block;
    padding:15px;
    margin:0 5px;
    color:#fff;
    background-color:#008CFB;
    text-decoration:none;
}
.days a.today{
    background-color:#017D27;
}
.days a.clicked{
    border:1px solid #fff;
    box-shadow:5px 5px 8px 0px #aaa;
}
.clicked-desc{
    color:#000;
    width:auto;
}
.clicked-desc strong{
    color:#008CFB;
}
</style>
</head>
<body>
<?php
$days=array();
$days[1]="Monday";
$days[2]="Tuesday";
$days[3]="Wednesday";
$days[4]="Thursday";
$days[5]="Friday";
$days[6]="Saturday";
$days[7]="Sunday";
?>
<div class="container">
    <div class="today">Today is <strong><?php echo date('l') ?></strong></div>
    <div class="days" id="days">
        <?php
        foreach($days as $key=>$val){
            ?><a href="javascript:void(0)" class="<?php if($val==date('l')){ echo "today"; }?>"><?php echo $val ?></a><?php
        }
        ?>
    </div>
    <div class="clicked-desc" id="clicked-desc"></div>
</div>
<script>
$("#days a").click(function() {
    $('#days a').removeClass('clicked');
    $(this).addClass('clicked');
    $("#clicked-desc").html("Selected day is <strong>"+($(this).text())+"</strong>");
});
</script>
</body>